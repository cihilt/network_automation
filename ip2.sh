#!/bin/bash
#http://pastebin.test.redhat.com/747924
#set -x
#tabs 3

# https://medium.com/@robaboukhalil/the-weird-wondrous-world-of-bash-arrays-a86e5adf2c69
# https://www.unix.com/shell-programming-and-scripting/199479-loop-through-array-arrays-string-spaces.html
# https://gitlab.cee.redhat.com/tools.gss.redhat.com/support-scripts/blob/master/sbr-networking/monitor2.sh


IP_address="sos_commands/networking/ip_address"
IP_d_address="sos_commands/networking/ip_-d_address"
SYSCTL="sos_commands/kernel/sysctl_-a"
CMDLINE="proc/cmdline"
FREE="sos_commands/memory/free"
MEMINFO="proc/meminfo"
LIMITS="etc/security/limits.conf"
NETSTAT="sos_commands/networking/netstat_-s"

check_netstat() {

        echo "Checking netstat for protocol isues..."

        for check in "${netstat_array[@]}"
        do
                netstat_type=$(echo $check | awk -F "|" '{print $1}')
                netstat_error=$(echo $check | awk -F "|" '{print $2}')
                netstat_total=$(echo $check | awk -F "|" '{print $3}')
                netstat_kcs=$(echo $check | awk -F "|" '{print $4}')

## This works by parsing the first column of array, then 2nd 
# netstat_array[1]="Ip
# reassembles failed
# Example: sed -n '/^Ip:/,/^[A-Z]/p' sos_commands/networking/netstat_-s | awk '($0 ~ "reassemblies required") {print $1}'

		error_count=$(sed -n "/^$netstat_type:/,/^[A-Z]/p" $NETSTAT | awk '($0 ~ "'"${netstat_error}"'") {print $1}')
                total_count=$(sed -n "/^$netstat_type:/,/^[A-Z]/p" $NETSTAT | awk '($0 ~ "'"${netstat_total}"'") {print $1}')

                if [[ $error_count -gt 0 ]]
                then
                        error_percent=$(echo "scale=5;$error_count/$total_count*100" | bc)
                        echo " Possible $netstat_type protocol issues found:"
                        echo "  $error_count $netstat_error out of $total_count $netstat_total ($error_percent percent)"
                        echo "  Check https://access.redhat.com/solutions/$netstat_kcs for more information"
                else
                        echo " No $netstat_type protocol issues found"
                fi
        done
echo ""
}

netstat_array[1]="Ip|reassembles failed|reassemblies required|891273"
netstat_array[2]="Udp|packet receive errors|packets received|26035"



# if ip address empty 
if [[ -e "$IP_address"  ]];then
	ipsexist="$IP_address"
	ovs=$(egrep ovs-system "$IP_address")
	IPS=$(cat $IP_address | grep -Eo "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"  | egrep -v "255|127.0.0")
elif [[ -e "$IP_d_address" ]];then
	echo "";
	ipsexist=$IP_d_address
	ovs=$(egrep ovs-system "$IP_d_address")
	IPS=$(cat $IP_d_address | grep -Eo "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"  | egrep -v "255|127.0.0")
else 
	echo -e "sos_commands\/networking\/ip_address doesnt exist, cannot detect ip address, exiting";
	exit;
fi

if [[ ! -z $ovs ]];then
	echo "ovs system detected, development still in progress" 
	#echo -e "xsos . | egrep "Uptime|Sys time|Hostname|Distro|Booted kernel:|Time Zone|Taint|LoadAvg|SEL" | awk '{$1=$1};1'" 
	#echo -e "xsos --net . | sed -n -e '/LSPCI/,/IP6/p' "
	exit
else
	echo -e "###### Environment Info ######\n";
	xsos . | egrep "Uptime|Sys time|Hostname|Distro|Booted kernel:|Time Zone|Taint|LoadAvg|SEL" | awk '{$1=$1};1'
	echo "$(grep -oP irqbalance ps)  is running\n"
	slaves=$(egrep master "$ipsexist" | awk -F: '{print $2}')
	noslavesip=$(egrep inet[^0-9] sos_commands/networking/ip_-o_addr | awk '{print $2}' | egrep -v lo | sort -u)
	nonbonds=$(egrep inet[^0-9] sos_commands/networking/ip_-o_addr | awk '{print $2}' | egrep -v "lo|bond" | sort -u)
	xsosips=$(xsos -i  . | sed -n -e '/IP4/,/IP6/p' | egrep -v "Warn|IP6|lo")
	pcaps=( $(find ../ -type f  -iname '*.pcap*') );
	hostname=$(cat ./hostname);
	monitorsh=( $(find ../ -type d  -iname '*network_stats*' | egrep $hostname) );
fi

# Manual way to get sostime, currently this function is not called.
sostime () {
rhel7time=$(egrep systemd-timedated sos_commands/systemd/systemctl_list-unit-files 2>/dev/null)
[[ -r etc/sysconfig/clock ]] && timezone=$(gawk -F= '/^ZONE=/{print $2}' "etc/sysconfig/clock" 2>/dev/null | tr -d \")
[[ ! -z "$rhel7time" ]] && timezone=$(readlink etc/localtime | awk -F/zoneinfo/ '{print $2}' 2>/dev/null)
       echo "Sosreport timezone is: $timezone "
}

# Manual way to get kernel, currently this function is not called
function manual_kernel {
rhel7time=$(egrep systemd-timedated sos_commands/systemd/systemctl_list-unit-files 2>/dev/null)
echo -e "\nRhel version is: $(cat etc/redhat-release)"
echo "kernel is:  $(cat uname | awk '{print $3}')"
sostime
echo "Hostname is: $(cat hostname)"
}

# This function will look for problems


function serverissues {
	servConntrack=$(grep "table full, dropping packet" sos_commands/kernel/dmesg var/log/messages | sort -u | head -n 1)

	if [[ $servConntrack ]];then
                echo -e "nfconntrack error detected, for Resolution see: https://access.redhat.com/solutions/8721\n"
        fi


check_netstat


}

function pcaps {
#####  Parse the pcaps, using the ips obtained.
        if [ ${#pcaps[@]} -eq 0 ]; then
                        echo "";
        else
		echo "The following pcaps are found: ${pcaps[@]}"
                echo -e "\r\n---------------- Start pcap analysis  ------------\n";
                for p in ${pcaps[@]};
                        do echo ${p};for l in $IPS;
                                do if [[ ! -n `tshark -r $p -qz conv,tcp 2>/dev/null | egrep $l` ]];then echo -e "Not Found IP $l in $p";
                                
				else 
					printf "\nFound IP $l in $p\n";
					printf "%s%s`tshark -r $p -qz expert,"ip.addr==$l" 2>/dev/null | egrep -i  "retrans|Duplicate|out-of-order"`\n";
                                	printf "%s%s\n Streams with ip $l\n";
                                	tshark -r "$p" -Y "ip.addr==$l" -T fields -E separator="|" -e "tcp.stream" 2>/dev/null | sort -u | xargs
                                fi;
                                echo -e "\r";
                      done;
                done;
        fi;
}

function packetissues {

	ETHTOOL_S="sos_commands/networking/ethtool_-S_$i"
	ETHTOOLS_array[0]="rx_disc|2127401"
	ETHTOOLS_array[1]="tx_disc|2127401"
	ETHTOOLS_array[2]="ring full|1751293"
	ETHTOOLS_array[3]="rx_crc_errors|66003"
	ETHTOOLS_array[4]="rx OOB|1751293"
	ETHTOOLS_array[5]="tx_pause_frames|1437443"
	ETHTOOLS_array[6]="rx_global_pause|1437443"
	ETHTOOLS_array[7]="rx_no_bufs|387813"
	ETHTOOLS_array[8]="rx_drop|387813"
	ETHTOOLS_array[9]="dropped rx|1751293"
	ETHTOOLS_array[10]="rx_missed_errors|28368"
	ETHTOOLS_array[11]="rx_address_mismatch_drops|274973"
	nic_hang=$(egrep "[rt]x hang" var/log/messages.filter | egrep $i)
	
}



function formulasreplication {
	packetissues
	echo -e "############## $i ################\n";
	echo "Is the Link Detected? $(egrep -i "link de" sos_commands/networking/ethtool_$i | grep -oP "no|yes")";
#old way    printf "The Driver and firmware is:%s\n`egrep -A1 "driver" sos_commands/networking/ethtool_-i_$i`\n";
	drivername=$(awk -F": " '{print $2}' sos_commands/networking/ethtool_-i_$i | head -n1)
#	egrep enic var/log/dmesg | grep -Po "[0-9][0-9]\:[0-9][0-9]\.[0-9]" | sort -u
        printf "%s\n" "----------- Driver Info -------------------";
	if [[ $(xsos --ethtool .  | egrep vif) ]];then 
		echo "vif detected, cannot get nic name";
	else
		egrep $(xsos --ethtool  . | egrep $i | awk '{print substr ($3,6)}') ./lspci 2>/dev/null
	fi
	xsos --ethtool  . | sed -n -e '/Interface Status:/,/Interface Errors:/p' | egrep $i | xargs
	#echo  "$nicname" 
	egrep "$drivername" -B1 sos_commands/kernel/modinfo* | egrep filename;
	echo -e "driver in lsmod:\n $(egrep "$drivername" lsmod)";
        echo -e "\rIs Arp filter on? $(egrep net.ipv4.conf.$i.arp_filter sos_commands/kernel/sysctl_-a)\n";
	xsos . --net 2>/dev/null | sed -n -e '/NETDEV/,/- - -/p' | egrep $i | sed '1iInterface  RxMiBytes  RxPackets  RxErrs  RxDrop  RxFifo   RxComp  RxFrame  RxMultCast\n=========  =========  =========  ======  =====    ======  ======     ======  =======  ' 2>/dev/null 
	echo -e "\n"
	xsos . --net 2>/dev/null | sed -n -e '/NETDEV/,/SOCKSTAT/p' | egrep $i | sed -n 2p | sed '1iInterface  TxMiBytes  TxPackets  TxErrs   TxDrop  TxFifo       TxComp  TxColls    TxCarrier\n=========  =========  =========  ======   ======  ======       ======  =======    ==========' 2>/dev/null
echo ""

	if [[ $nic_hang  ]];then
		echo "tx hang detected on recent messages, for Resolution see: https://access.redhat.com/solutions/2837431"
	fi 

###### NIC drops

function ETHTOOL_check {

for check in "${!ETHTOOLS_array[@]}"
        do
                ETHTOOLS_error=$(printf '%s ' ${ETHTOOLS_array[$check]} | awk -F "|" '{print $1}')
                ETHTOOLS_kcs=$(echo   ${ETHTOOLS_array[$check]} | awk -F "|" '{print $2}')
		error_count=$(sed -n "/[^:]\s$ETHTOOLS_error/p" $ETHTOOL_S | awk -F":" '($0 ~ "'"${ETHTOOLS_error}"'") {print $2}'  |  paste -sd+ | bc)
                #error_count=$(awk '($0 ~ "'"${ETHTOOLS_error}"'") {print $2}' $ETHTOOL_S | egrep ^[0-9])
		#total_count=$(sed -n "/[^:]\s$ETHTOOLS_total:/p" $ETHTOOL_S | awk '($0 ~ "'"${ETHTOOLS_total}"'") {print $2}')
                rxtotal_pkt=$(egrep $i  proc/net/dev | awk '{print $2}')
                txtotal_pkt=$(egrep $i  proc/net/dev | awk '{print $11}')
		if [[ ${ETHTOOLS_error} =~ .*rx.* ]];then
			total_pkt=$(egrep $i  proc/net/dev | awk '{print $2}')                
		else
			total_pkt=$(egrep $i  proc/net/dev | awk '{print $11}')
		fi
		if [[ $error_count -gt 0 ]]
                then
			#echo -e "\r\n---------------- $i Packet Loss found ------------\n";
                        error_percent=$(echo "scale=5;$error_count/$total_pkt*100" | bc)
                        echo "  $error_count $ETHTOOLS_error out of $total_pkt $ETHTOOLS_total ($error_percent percent)"
                        echo "  Check https://access.redhat.com/solutions/$ETHTOOLS_kcs for more information"
		else
			echo ""		
#			echo "$ETHTOOLS_error"
                fi
done

}

ETHTOOL_check

# Obselete [[ $(awk '/RX\:/{print $2}' sos_commands/networking/ethtool_-g_$i | sort -u | wc -l) -gt 1 ]] && echo -e "Can Adjust Ring Buffer with Resolution: https://access.redhat.com/solutions/2127401";


##### Check monitor.sh for errors
	if [ ${#monitorsh[@]} -eq 0 ];then
			echo -e "";
	else
		echo -e "\r\n-------------- start monitor.sh analysis ----------\n";
		#echo " Customer uploaded the following monitor.sh $monitorsh";
		for s in $monitorsh;
			do egrep "file|(drop|disc|err|fifo|buf|fail|miss|OOB|fcs|full|frags|hdr|tso|pause|lost).*: [^0]" $s/sys_statistics_$i
		done;
	fi;

}



function converse {
        printf "%s\n" "TCP Conversations" 
        printf "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t%s\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t%s\n" "|       <-      | |       ->      | |     Total     |    Relative    |   Duration   | " "| Frames  Bytes | | Frames  Bytes | | Frames  Bytes |      Start     |              | "
}


if [ -z "$xsosips" ];then 
	echo "empty";
	
else 
	echo "$xsosips";
	echo -e "\r"
fi

serverissues

#### First Formula ####### If there are bonding slaves  & ips are empty
if [[ ! -z "$slaves" && -z "$nonbonds" ]];then  #// if $slaves is not empty
	for i in $slaves;
		do echo "first formula";
		formulasreplication; 
	done;


#### Second Formula - If there are bonding slaves & ips are NOT empty  ################
elif [[ ! -z "$slaves" && ! -z "$nonbonds" ]];then
        for i in $nonbonds $slaves;
                do echo "2nd formula"  
                formulasreplication;
        done;

#### Third Formula - If there are no bonding slaves ################
elif [[ ! -z "$noslavesip" ]];then
	for i in $nonbonds;
                do echo "3rd formula"  
                formulasreplication;
        done;

##### Interface not found ##########
else
 	echo "$i interface variable is empty, cannot obtain info, exiting";
	exit
fi;

pcaps
# Author: Jon Nikolakakis -> jnikolak@redhat.com  
# Version: Beta
# Usage: Run inside a sosreport, and place the script two folders before                 
#        Then do: ../../script.sh
#                                
# What does the script do: 
#	Its focused on getting information that is relevant to networking cases using xsos commands. This is more like a xsos plugin with some
#       add features. 

#	It will find network related issues then provide the kcs solution to resolve them.
#	If a apcket capture is provided onto the parent folder of the sosreport, it will find them and check if the ip's from the sosreport match the acket captures.
#	Then it will display the related streams associated with the packet capture.

#	FAQ:
#	Why doesnt support ovs-system? Its because I need to develop another way to parse this information, that is unique to ovs, its in development.
#	Why not just update xsos with these scripts?  If there is enough interest, I can do it.
#	Why write this in bash, I'm more confortable in bash at the moment than in python, but it went be a good project to convert it to python.
#	Why not use Access Insights? I think it would better to use AI but that may take a while to develop and I needed something that I could quickly apply on cases. 

##### Notes
# IP fragmentation happens because incoming fragments are queued until all fragments for a full packet arrive then it's assembled and passed up to the next layer e.g UDP. But if all packets have not arrived within "net.ipv4.ipfrag_time" which is 30 seconds. Then the IP stack will drop the rest of the fragments and increment the above counters.


### Future ###
# Ability to scrape through *relevant* streams and find issues in the pcaps.
# Ability to add this script to supportshell
# https://stackoverflow.com/questions/3685970/check-if-a-bash-array-contains-a-value